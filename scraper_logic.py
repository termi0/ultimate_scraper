# -*- coding: utf-8 -*-
"""
Created on Sun Jun 17 16:30:10 2018

@author: termi
"""

from pathlib import Path
import os
   
page = "1"
configFromTxt = []

class logic:
    def savePathWhereToScrape():
        save_path = input("Podaj sciezke gdzie maja byc zapisywane memy (program domyslnie tworzy folder do zapisu memow):")
        if save_path == "":
            save_path = "Kwejk"
            toFile = "save_path=" + str(save_path)
            logic.saveToFile(toFile)        
            return save_path
        else:
            toFile = "save_path=" + str(save_path)
            logic.saveToFile(toFile)        
            return save_path
        '''function ask user where he want to save his file and add path to the config file'''
        
    def startingPageToScrap():
        while True:
            try:
                startingPage = int(input("Podaj numer strony na której program ma zaczac skrapowanie:"))
                toFile = "\n" + "starting_page=" + str(startingPage)
                logic.saveToFile(toFile)                  
                return startingPage
                break
            except ValueError:
                print("podana wartosc nie jest prawidlowa, prosze podac liczbe wieksza od 0")
        '''function ask user on witch page he want to start scrape mem'''
         
    def numberOfIteration():
        while True:
            try:
                numberOfInteration = int(input("Ile stron chciałbys sciagnac, podaj liczbe wieksza od 0:"))
                toFile = "\n" + "number_of_iteration=" + str(numberOfInteration)
                logic.saveToFile(toFile)                
                return numberOfInteration
                break
            except ValueError:
                print("podana wartosc nie jest prawidlowa, prosze podac liczbe wieksza od 0")
        '''Function determinate how mane page script will scrape'''
        
    def delayOnScrape():
        while True:
            try:
                delay = input("podaj opóźnienie w scrapowaniu następnej strony (opóźnie jest w sekundach, domyslnie wynosi ono 0s):")
                if delay == "":
                    delay = 0
                    toFile = "\n" + "delay=" + str(delay)
                    logic.saveToFile(toFile)                
                    return delay
                    break
                else:
                    toFile = "\n" + "delay=" + str(delay)
                    logic.saveToFile(toFile)                
                    return int(delay)
                    break
            except ValueError:
                print("podana wartosc nie jest prawidlowa, prosze podac liczbe wieksza lub równą 0")
        '''Function ask user to define delay on main loop'''
        
    def lastScrapingPage(currentScrapPage): 
        global page
        if  page == "1":
            page ="\n" + "last_scraped_page=" + str(currentScrapPage)
            logic.saveToFile(page)
        else:
            newPage ="\n" + "last_scraped_page=" + str(currentScrapPage)
            with open("config.txt", "r") as file:
                config = file.read()
                file.close()
            config = config.replace(page, newPage)            
            with open("config.txt", "w") as file:
                file.write(config)                 
                file.close()
            page = newPage
        '''Function take starting page define by user and write last scraping page to the config file'''        
             
    def saveToFile(whatToSave):        
        #tworzenie pliku config dla programu
        config = open("config.txt", "a")
        config.write(whatToSave)
        config.close()
        '''Function take data and save it to config file'''
        
    def checkForConfig():
        my_config = Path("config.txt")
        if my_config.is_file():
            return True
        '''Function will check if config exist, if config exist it will return true'''
        
    def loadConfigFromFile():
        global configFromTxt
        global testTablicy
        with open("config.txt", "r") as config:  
            for line in enumerate(config):                
                dataSetScraper = line[1].split("=")
                dataSetScraper[1].strip()
                configFromTxt.append(dataSetScraper[1])
            
        return configFromTxt
        '''Function load config from file'''
    
    def checkForNumberOfIterationLeft(declaretIteratio, lastScrapedPage, startedPage):
        numberOfIteration = startedPage - lastScrapedPage
        iterationLeft = declaretIteratio - numberOfIteration - 1
        return iterationLeft
        '''function calculation a number of iteration left when user load config'''
    
    def deleteOldConf():     
        os.remove("config.txt")
        '''Function is removing old config'''
        

