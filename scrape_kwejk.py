# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 08:36:05 2018

@author: termi"""    
import requests
# Alternative 1 Temporarily change your working directory #
import os
#os.chdir("**Put here the directory where you have the file with your function**")
os.chdir(".")
from scraper_logic import logic
#os.chdir("**Put here the directory where you were working**")
os.chdir(".")
from time import sleep
from bs4 import BeautifulSoup

configFromTxt =[]
url = "https://kwejk.pl/strona/"

#check for config file
if logic.checkForConfig() == True:
    
   #User decide if he want to load config from file
   load_config = str(input("znalezionio plik configuracyjny, czy chcesz go uzyc? T/n "))
   
   #Load config frome file and set all the variable
   if load_config == "t" or load_config == "T" or load_config == "":
      configFromTxt = logic.loadConfigFromFile()
      
      save_path = configFromTxt[0]
      save_path.rstrip()
      
      startingPageToScrap = int(configFromTxt[1])
      
      numberOfInterationFromFile = int(configFromTxt[2])      
      
      last_scraped_page = int(configFromTxt[4])
      
      #Calculation number of interation to end task
      numberOfInteration = logic.checkForNumberOfIterationLeft(numberOfInterationFromFile, last_scraped_page, startingPageToScrap)
      
      delay = int(configFromTxt[3])
      
      currentScrapPage = last_scraped_page
      
   #Creating new config
   else:
        #Delete old config
        logic.deleteOldConf()
       
        #User define where he want to save files
        save_path = logic.savePathWhereToScrape()

        #User define on witch page he want to start scrape
        startingPageToScrap = logic.startingPageToScrap()     

        #Aditional info for user where the program end his work and on witch page it is right now   
        currentScrapPage = startingPageToScrap

        #User define number of iteraion
        numberOfInteration = logic.numberOfIteration()

        #User define delay on scraping
        delay = logic.delayOnScrape()
else:
    #User define where he want to save files
    save_path = logic.savePathWhereToScrape()

    #User define on witch page he want to start scrape
    startingPageToScrap = logic.startingPageToScrap()     

    #Aditional info for user where the program end his work and on witch page it is right now   
    currentScrapPage = startingPageToScrap

    #User define number of iteraion
    numberOfInteration = logic.numberOfIteration()

    #User define delay on scraping
    delay = logic.delayOnScrape()

if numberOfInteration == 0:
   input("nie ma juz co skrapowac")
    
else:

    for x in range(numberOfInteration):
        
        logic.lastScrapingPage(currentScrapPage)
    
        #przerobienie nr strony do stringa     
        currentScrapingPageSTR = str(currentScrapPage)
    
        #laczenie adresu strony z obecna strona do skrapowania    
        my_urlText = ''.join([url,currentScrapingPageSTR])    
        my_url = requests.get(my_urlText)
    
        #strona jako text do szukania obrazkow
        data = my_url.text
    
        #debug
        print("Strona która jest obecnie skrapowana:",currentScrapPage)  

        #hml parser
        soup = BeautifulSoup(data,"lxml")
    
        #szukanie obrazkow na stronie
        for link in soup.find_all('img'):
        
            #pobieranie ich src
            image = link.get("src")
        
            #uzyskanie nazwy obrazka
            image_name = os.path.split(image)[1]   
        
            if "i1.kwejk.pl/k/obrazki" in image:
            
                #tworzenie nazwy dla pliku oraz jego sciezki zapisu            
                pathAndName = '/'.join([save_path.rstrip(),image_name])
                if not os.path.exists(save_path.rstrip()):
                    os.makedirs(save_path.rstrip())
            
                downloadedJpg = requests.get(image)
            
                #zapis pliku
                with open(pathAndName, "wb") as myJpg:
                    myJpg.write(downloadedJpg.content)
                 
                    #dekrementacja strony do skrapowania             
                    currentScrapPage = currentScrapPage-1
                    sleep(delay) #in seconds
    
        if x == numberOfInteration - 1:
            input("skrapowanie zostalo zakonczone")
     
          
